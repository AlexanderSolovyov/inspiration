from django.contrib.auth.models import User
from django.http import JsonResponse

from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView
from rest_framework.generics import ListAPIView
from rest_framework.parsers import FormParser
from rest_framework.parsers import JSONParser
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from unsplash.models import SearchHistory
from unsplash.serializers import SearchHistorySerializer
# from users.serializers import PictureUserProfileSerializer
from users.models import UserProfile
from users.serializers import PictureUserProfileSerializer
from users.serializers import RegisterSerializer
from users.serializers import UserSerializer


class RegisterView(CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


class UserProfileViewSet(ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    @action(
        methods=["POST"],
        detail=False,
        parser_classes=[MultiPartParser],
        serializer_class=PictureUserProfileSerializer,
        permission_classes=[permissions.IsAuthenticated],
    )
    def upload_profile_picture(self, request, *args, **kwargs):
        """To save the profile picture"""
        user = self.request.user
        try:
            user_profile = user.profile
        except UserProfile.DoesNotExist:
            user.profile = UserProfile.objects.create(user=user, full_name=user.username)
            user_profile = user.profile
        # Formatting the data to as per our defined serializer

        # Serializing our data
        ser = PictureUserProfileSerializer(
            user_profile, data=request.data, context={"request": request}
        )

        if ser.is_valid():
            if ser.validated_data:
                # Deleting the old image before uploading new image
                if user_profile.pic:
                    user_profile.pic.delete()

                # Saving the model
                ser.save()
            return_ser_data = {"user": ser.data.get("user"), "pic": ser.data.get("pic")}
            return Response(return_ser_data, status=status.HTTP_200_OK)
        else:
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)


class CurrentUserView(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    def get(self, request):
        serializer = UserSerializer(request.user)
        return JsonResponse(serializer.data)


class SearchHistoryView(ListAPIView):
    permission_classes = [
        IsAuthenticated,
    ]
    model = SearchHistory
    serializer_class = SearchHistorySerializer

    def get_queryset(self):
        return SearchHistory.objects.filter(user=self.request.user).order_by(
            "-created"
        )[:10]
