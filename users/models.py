from django.contrib.auth.models import User
from django.db import models

from model_utils.models import TimeStampedModel


class UserProfile(TimeStampedModel):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True, related_name="profile"
    )

    pic = models.ImageField(null=True, blank=True)
    full_name = models.CharField(max_length=100)
