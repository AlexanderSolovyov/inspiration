# Generated by Django 3.1.6 on 2021-03-07 22:20

from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("users", "0004_auto_20210302_1446"),
    ]

    operations = [
        migrations.AlterField(
            model_name="userprofile",
            name="pic",
            field=models.ImageField(blank=True, null=True, upload_to=""),
        ),
    ]
