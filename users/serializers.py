from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from users.models import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        read_only_fields = (
            "created_at",
            "updated_at",
        )
        exclude = ("user",)


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True, validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    password2 = serializers.CharField(write_only=True, required=True)
    profile = UserProfileSerializer(required=False)

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "password",
            "password2",
            "email",
            "first_name",
            "last_name",
            "profile",
        )

    def validate(self, attrs):
        if attrs["password"] != attrs["password2"]:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."}
            )

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data["username"],
            email=validated_data["email"],
        )
        if validated_data.get("first_name") is not None:
            user.first_name = validated_data["first_name"]
        if validated_data.get("last_name") is not None:
            user.last_name = validated_data["last_name"]

        user.set_password(validated_data["password"])
        user.save()

        if "profile" in validated_data:
            profile_data = validated_data.pop("profile")
            profile_data["user"] = user
            UserProfile.objects.get_or_create(**profile_data)

        return user


class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer()

    class Meta:
        model = User
        write_only_fields = ("password",)
        fields = (
            "id",
            "username",
            "email",
            "first_name",
            "last_name",
            "password",
            "profile",
        )

    def update(self, instance, validated_data):
        instance.username = validated_data.get("username", instance.username)
        instance.email = validated_data.get("email", instance.email)
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.save()

        if "profile" in validated_data:
            profile_data = validated_data.get("profile")
            profile = instance.profile
            profile.full_name = profile_data.get("full_name", profile.full_name)
            profile.save()

        return instance


class PictureUserProfileSerializer(serializers.ModelSerializer):
    """Serializer for user image upload"""

    pic = serializers.ImageField(allow_null=True, use_url=True)

    def create(self, validated_data):
        validated_data["user"] = self.context["request"].user
        return super(PictureUserProfileSerializer, self).create(validated_data)

    class Meta:
        model = UserProfile
        fields = ("user", "pic")
        read_only_fields = ("user",)
