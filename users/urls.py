from django.conf.urls import include
from django.conf.urls import url
from django.urls import path

from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_nested import routers

from users.views import CurrentUserView
from users.views import RegisterView
from users.views import SearchHistoryView
from users.views import UserProfileViewSet

router = routers.SimpleRouter()

router.register(r"profile", UserProfileViewSet, basename="user_profile")

urlpatterns = [
    url(r"", include(router.urls)),
    path("search-history/", SearchHistoryView.as_view(), name="search_history"),
    path("register/", RegisterView.as_view(), name="auth_register"),
    path("login/", obtain_auth_token, name="api_token_auth"),
    path("me/", CurrentUserView.as_view(), name="current_user"),
]
