from django.http import JsonResponse

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from unsplash.models import SearchHistory
from unsplash.serializers import SearchSerializer
from unsplash.utils import UnsplashClient

query_param = openapi.Parameter(
    "query", in_=openapi.IN_QUERY, description="Query", type=openapi.TYPE_STRING
)
page_param = openapi.Parameter(
    "page",
    in_=openapi.IN_QUERY,
    description="Page",
    type=openapi.TYPE_INTEGER,
    required=False,
)
order_by_param = openapi.Parameter(
    "order_by",
    in_=openapi.IN_QUERY,
    description="Order By",
    type=openapi.TYPE_STRING,
    required=False,
)
orientation_param = openapi.Parameter(
    "orientation",
    in_=openapi.IN_QUERY,
    description="Orientation",
    type=openapi.TYPE_STRING,
    required=False,
)


# Create your views here.
class ImageSearch(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    @swagger_auto_schema(
        manual_parameters=[query_param, page_param, order_by_param, orientation_param],
        query_serializer=SearchSerializer,
        responses={"400": "Bad Request"},
        security=[],
        operation_id="Search unsplash",
        operation_description="This endpoint does some magic",
    )
    def get(self, request, *args, **kwargs):
        ser = SearchSerializer(data=request.GET)
        ser.is_valid(raise_exception=True)
        query = request.GET.get("query")
        order_by = request.GET.get("order_by")
        orientation = request.GET.get("orientation")

        try:
            page = int(request.GET.get("page", 1))
        except ValueError:
            page = request.GET.get("page")
        if not isinstance(query, str):
            return JsonResponse({"error": "query field required"}, status=400)
        if not isinstance(page, int):
            return JsonResponse({"error": "page must be int value"}, status=400)

        unsplash_client = UnsplashClient()
        response = unsplash_client.search_image(
            query=query, page=page, order_by=order_by, orientation=orientation
        )
        SearchHistory.objects.filter(
            user=self.request.user,
            query=query,
            order_by=order_by,
            orientation=orientation,
        ).delete()
        SearchHistory.objects.create(
            user=self.request.user,
            query=query,
            order_by=order_by,
            orientation=orientation,
        )
        return JsonResponse(response)
