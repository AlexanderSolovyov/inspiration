from django.conf import settings

import requests


class UnsplashClient(object):
    base_url: str
    access_key: str

    def __init__(self):
        self.base_url = settings.UNSPLASH_API_URL
        self.access_key = settings.UNSPLASH_PUBLIC_KEY

    def _build_headers(self):
        return {
            "Authorization": f"Client-ID {self.access_key}",
            "Accept-Version": "v1",
        }

    def retrieve_url(self, path: str, params: dict):
        return requests.get(
            url=self.base_url + path,
            params=params,
            headers=self._build_headers(),
        ).json()

    def search_image(
        self,
        query: str,
        page: int,
        order_by: str = "relevant",
        orientation: str = "landscape",
    ) -> dict:
        """
        /search/photos?query=
        :param query: string
        :param page: int
        :return: dict
        """
        order_by = order_by or "relevant"
        orientation = orientation or "landscape"
        return self.retrieve_url(
            path="search/photos",
            params={
                "query": query,
                "page": page,
                "order_by": order_by,
                "orientation": orientation,
            },
        )
