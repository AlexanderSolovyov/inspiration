from django.urls import path
from django.views.decorators.cache import cache_page

from unsplash.views import ImageSearch

urlpatterns = [
    path("search/", cache_page(60 * 60)(ImageSearch.as_view()), name="image_search"),
]
