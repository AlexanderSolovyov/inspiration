from abc import ABC

from rest_framework import serializers

from unsplash.models import SearchHistory


# TODO: delete or use
class SearchSerializer(serializers.Serializer):
    query = serializers.SerializerMethodField()
    page = serializers.SerializerMethodField(required=False)
    order_by = serializers.SerializerMethodField(required=False)
    orientation = serializers.SerializerMethodField(required=False)

    def get_query(self, obj):
        return self.context["request"].query_params.get("query", None)

    def get_page(self, obj):
        return self.context["request"].query_params.get("query", None)

    def get_order_by(self, obj):
        return self.context["request"].query_params.get("order_by", None)

    def get_orientation(self, obj):
        return self.context["request"].query_params.get("orientation", None)


class SearchHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SearchHistory
        fields = ("query", "order_by", "orientation")
