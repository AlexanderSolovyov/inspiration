from django.contrib.auth.models import User
from django.db import models

from model_utils.models import TimeStampedModel


# Create your models here.
class SearchHistory(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    query = models.CharField(max_length=100)
    order_by = models.CharField(max_length=100, null=True, blank=True)
    orientation = models.CharField(max_length=100, null=True, blank=True)

    class Meta:
        unique_together = ("user", "query", "order_by", "orientation")
