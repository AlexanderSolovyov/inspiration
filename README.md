# Inspiration Backend
[![pipeline status](https://gitlab.com/AlexanderSolovyov/inspiration/badges/master/pipeline.svg)](https://gitlab.com/AlexanderSolovyov/inspiration/-/commits/master)
[![coverage report](https://gitlab.com/AlexanderSolovyov/inspiration/badges/master/coverage.svg)](https://gitlab.com/AlexanderSolovyov/inspiration/-/commits/master)
---

## Stress Testing

#### Apache benchmark

```
ab -n 20000 -c 100 -m "GET" -H "Authorization:Token f4012ff659961dfa91883b757166746e9adc557a" http://enigmatic-scrubland-70991.herokuapp.com/api/users/search-history/
```
![](stress_testing/apache_bench.png)

##### Failure Intensity:
```
MTTF = time / # of failures:
MTTF = 428206 ms / 7910 = 54.1347661188

FI = 1 / MTTF
FI = 1 / 54.1347661188 ≈ 0.018
```


#### Artillery
![](stress_testing/artillery1.png)
![](stress_testing/artillery2.png)


#### Heroku dyno load
![](stress_testing/dyno_load.png) 

---

### Formatting:
```
black .
isort -sl -m 3 -y
```


### Init setup:
```
make build
make run
make migrate
```

### Admin:
http://localhost:8000/admin/

log:  `test` \
pass: `test`
