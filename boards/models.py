from django.contrib.auth.models import User
from django.db import models

from model_utils.models import TimeStampedModel


# Create your models here.
class BoardTheme(TimeStampedModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Board(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    is_private = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    class Meta:
        unique_together = ("user", "name")

    def __str__(self):
        return f"User: {self.user_id}; Board: {self.name}"


class BoardItem(TimeStampedModel):
    board = models.ForeignKey(Board, on_delete=models.CASCADE, related_name="items")
    image = models.JSONField(default=dict)
    unsplash_id = models.CharField(max_length=100)

    class Meta:
        unique_together = ("board", "unsplash_id")

    def __str__(self):
        return f"User: {self.board.user_id}; Board: {self.board.name}"


class Rating(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("user", "board")
