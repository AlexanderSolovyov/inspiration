from django.conf.urls import include
from django.conf.urls import url
from django.urls import path

from rest_framework_nested import routers

from boards.views import BoardItemViewSet
from boards.views import RelevantBoardsView
from boards.views import UserBoardsViewSet

router = routers.SimpleRouter()


router.register("", UserBoardsViewSet, basename="user_boards")
boards_router = routers.NestedSimpleRouter(router, r"", lookup="board")
boards_router.register(r"items", BoardItemViewSet, basename="board-items")

urlpatterns = [
    path("relevant-boards/", RelevantBoardsView.as_view(), name="relevant_boards"),
    url(r"^", include(router.urls)),
    url(r"^", include(boards_router.urls)),
]
