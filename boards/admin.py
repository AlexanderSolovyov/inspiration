from django.contrib import admin

from boards.models import Board
from boards.models import BoardItem
from boards.models import BoardTheme
from boards.models import Rating

# Register your models here.

admin.site.register(Board)
admin.site.register(BoardTheme)
admin.site.register(Rating)
admin.site.register(BoardItem)
