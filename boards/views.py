from django.http import JsonResponse

import django_filters.rest_framework
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from boards.models import Board
from boards.models import BoardItem
from boards.serializers import BoardItemSerializer
from boards.serializers import BoardSerializer
from boards.serializers import RelevantBoardsItemSerializerRequest

unsplash_id_param = openapi.Parameter(
    "unsplash_id",
    in_=openapi.IN_QUERY,
    description="Unsplash ID",
    type=openapi.TYPE_STRING,
)


class UserBoardsViewSet(ModelViewSet):
    permission_classes = [
        IsAuthenticated,
    ]
    serializer_class = BoardSerializer
    model = serializer_class.Meta.model
    paginate_by = 10
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = {
        "created": ["gte", "lte", "exact", "gt", "lt"],
        "modified": ["gte", "lte", "exact", "gt", "lt"],
    }

    def get_serializer_context(self):
        return {"request": self.request}

    def get_queryset(self):
        return Board.objects.filter(user_id=self.request.user.id)


class BoardItemViewSet(ModelViewSet):
    permission_classes = [
        IsAuthenticated,
    ]
    serializer_class = BoardItemSerializer

    def get_queryset(self):
        return BoardItem.objects.filter(board=self.kwargs["board_pk"])


class RelevantBoardsView(APIView):
    permission_classes = [
        IsAuthenticated,
    ]

    @swagger_auto_schema(
        manual_parameters=[unsplash_id_param],
        query_serializer=RelevantBoardsItemSerializerRequest,
        responses={
            "200": BoardSerializer(many=True),
            "400": "Bad Request",
        },
        security=[],
        operation_id="RelevantBoardsView",
        operation_description="This endpoint does some magic",
    )
    def get(self, request, *args, **kwargs):
        request_ser = RelevantBoardsItemSerializerRequest(data=request.GET)
        request_ser.is_valid(raise_exception=True)

        if not request.GET.get("unsplash_id"):
            return JsonResponse({"error": "unsplash_id field required"}, status=400)

        relevant_boards = Board.objects.filter(
            items__unsplash_id=request.GET.get("unsplash_id"), user=self.request.user
        )
        return JsonResponse(
            BoardSerializer(relevant_boards, many=True).data, safe=False
        )
