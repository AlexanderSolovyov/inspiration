from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from boards.models import Board
from boards.models import BoardItem


class BoardSerializer(serializers.ModelSerializer):
    cover = SerializerMethodField(method_name="get_cover")

    # noinspection PyMethodMayBeStatic
    def get_cover(self, obj: Board):
        try:
            board_items = obj.items
            if board_items is not None:
                return board_items.latest("created").image or None
            else:
                return None
        except BoardItem.DoesNotExist:
            return None

    def create(self, validated_data):
        validated_data["user"] = self.context["request"].user
        return super(BoardSerializer, self).create(validated_data)

    class Meta:
        model = Board
        fields = "__all__"
        extra_kwargs = {"user": {"read_only": True}, "cover": {"read_only": True}}


class BoardItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = BoardItem
        fields = "__all__"


class RelevantBoardsItemSerializerRequest(serializers.Serializer):
    unsplash_id = serializers.SerializerMethodField()

    def get_unsplash_id(self, obj):
        return self.context["request"].query_params.get("unsplash_id", None)
