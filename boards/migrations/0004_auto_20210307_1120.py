# Generated by Django 3.1.6 on 2021-03-07 11:20

from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("boards", "0003_remove_boardtheme_user"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="board",
            name="theme",
        ),
        migrations.AddField(
            model_name="board",
            name="cover",
            field=models.JSONField(default=dict),
        ),
    ]
