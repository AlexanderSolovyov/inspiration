import json

from django.contrib.auth.models import User

from rest_framework import status

from tests.base import InspirationTestCase
from users.models import UserProfile


class UserProfileTestCase(InspirationTestCase):
    def setUp(self):
        super().setUp()
        self.new_user_email = "new_user@mail.com"
        self.new_user_username = "new_user"
        self.new_user_password = "new_user"
        self.new_user_fullname = "new_fullname"

    def test_register_new_user(self):
        registration_data = {
            "username": self.new_user_username,
            "email": self.new_user_email,
            "password": self.new_user_password,
            "password2": self.new_user_password,
            "profile": {"full_name": self.new_user_fullname},
        }

        assert not UserProfile.objects.exists()

        response = self.client.post(
            "/api/users/register/",
            data=json.dumps(registration_data),
            content_type="application/json",
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert UserProfile.objects.count() == 1
        assert UserProfile.objects.first().full_name == self.new_user_fullname
        assert UserProfile.objects.filter(
            user__username=self.new_user_username
        ).exists()

    def test_patch_user_profile(self):
        updated_data = {
            "username": self.new_user_username + "_patched",
            "profile": {"full_name": self.new_user_fullname + "_patched"},
        }

        assert not UserProfile.objects.exists()
        UserProfile.objects.create(
            user=self.fixtures.test_user, full_name="dummy_value"
        )
        self.client.patch(
            f"/api/users/profile/{self.fixtures.test_user.id}/",
            json.dumps(updated_data),
            content_type="application/json",
        )

        assert UserProfile.objects.exists()
        assert (
            UserProfile.objects.first().full_name == self.new_user_fullname + "_patched"
        )

    def test_current_user_view(self):
        response = self.client.get("/api/users/me/").json()
        assert response["username"] == "test1"
