from rest_framework import status
from rest_framework.test import APIRequestFactory

from tests.base import InspirationTestCase
from users.views import RegisterView


class RegisterTestCase(InspirationTestCase):
    def setUp(self):
        super().setUp()
        self.new_user_email = "new_user@mail.com"
        self.new_user_username = "new_user"
        self.new_user_password = "new_user"

    def test_register_new_user(self):
        factory = APIRequestFactory()
        registration_data = {
            "username": self.new_user_username,
            "email": self.new_user_email,
            "password": self.new_user_password,
            "password2": self.new_user_password,
        }
        request = factory.post("/api/users/register/", registration_data)
        response = RegisterView.as_view()(request)
        assert response.status_code == status.HTTP_201_CREATED

    def test_register_new_user_with_fullname(self):
        factory = APIRequestFactory()
        registration_data = {
            "username": self.new_user_username,
            "email": self.new_user_email,
            "password": self.new_user_password,
            "password2": self.new_user_password,
            "first_name": "Kek",
            "last_name": "Kekov",
        }
        request = factory.post("/api/users/register/", registration_data)
        response = RegisterView.as_view()(request)
        assert response.status_code == status.HTTP_201_CREATED

    def test_register_user_different_passwords(self):
        factory = APIRequestFactory()
        registration_data = {
            "username": self.new_user_username,
            "email": self.new_user_email,
            "password": self.new_user_password,
            "password2": "wrong_password",
        }
        request = factory.post("/api/users/register/", registration_data)
        response = RegisterView.as_view()(request)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_register_user_existed_email(self):
        factory = APIRequestFactory()
        registration_data = {
            "username": self.new_user_username,
            "email": "test1@mail.com",
            "password": self.new_user_password,
            "password2": self.new_user_password,
        }
        request = factory.post("/api/users/register/", registration_data)
        response = RegisterView.as_view()(request)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_register_user_existed_username(self):
        factory = APIRequestFactory()
        registration_data = {
            "username": "test1",
            "email": self.new_user_email,
            "password": self.new_user_password,
            "password2": self.new_user_password,
        }
        request = factory.post("/api/users/register/", registration_data)
        response = RegisterView.as_view()(request)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_register_user_password_less_8_symbols(self):
        factory = APIRequestFactory()
        registration_data = {
            "username": self.new_user_username,
            "email": self.new_user_email,
            "password": "kek",
            "password2": "kek",
        }
        request = factory.post("/api/users/register/", registration_data)
        response = RegisterView.as_view()(request)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_register_user_no_email(self):
        factory = APIRequestFactory()
        registration_data = {
            "username": self.new_user_username,
            "password": self.new_user_password,
            "password2": self.new_user_password,
        }
        request = factory.post("/api/users/register/", registration_data)
        response = RegisterView.as_view()(request)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_register_user_no_username(self):
        factory = APIRequestFactory()
        registration_data = {
            "email": self.new_user_email,
            "password": self.new_user_password,
            "password2": self.new_user_password,
        }
        request = factory.post("/api/users/register/", registration_data)
        response = RegisterView.as_view()(request)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
