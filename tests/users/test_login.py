from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.test import APIRequestFactory

from tests.base import InspirationTestCase


class LoginTestCase(InspirationTestCase):
    def test_success_login(self):
        factory = APIRequestFactory()
        login_data = {"username": "test1", "password": "test1"}
        request = factory.post("/api/users/login/", login_data)
        response = obtain_auth_token(request)
        assert response.status_code == status.HTTP_200_OK
        assert response.data.get("token") is not None

    def test_wrong_credentials(self):
        factory = APIRequestFactory()
        login_data = {"username": "wrong_username", "password": "wrong_password"}
        request = factory.post("/api/users/login/", login_data)
        response = obtain_auth_token(request)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data.get("token") is None
