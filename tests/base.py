from django.test import TestCase

import pytest
from mock import patch


@pytest.mark.usefixtures("inspiration_user")
class InspirationTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.client = self.fixtures.get_patched_client()

    def _post_teardown(self):
        super(InspirationTestCase, self)._post_teardown()
        patch.stopall()
