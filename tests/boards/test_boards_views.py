import json

from rest_framework import status

from boards.models import Board
from tests.base import InspirationTestCase
from users.models import User


class RegisterTestCase(InspirationTestCase):
    def setUp(self):
        super().setUp()
        self.test_board = Board.objects.create(
            name="Test cover", user=User.objects.get(username="test1")
        )
        self.test_board.save()

    def test_get_boards(self):
        response = self.client.get(
            "/api/boards/",
        )
        assert response.status_code == status.HTTP_200_OK

    def test_post_user_boards(self):
        response = self.client.post(
            "/api/boards/",
            data={
                "cover": json.dumps(
                    {"link": "https://www.youtube.com/watch?v=dQw4w9WgXcQ"}
                ),
                "name": "string",
            },
        )
        assert response.data["name"] == "string"
        assert response.status_code == status.HTTP_201_CREATED

    def test_put_boards(self):
        boards = self.client.get(
            "/api/boards/",
        ).data
        board_id = boards[0]["id"]
        assert boards[0]["name"] == "Test cover"
        response = self.client.put(
            f"/api/boards/{board_id}/", data={"name": "new name"}
        )
        assert response.data["name"] == "new name"
        assert response.status_code == status.HTTP_200_OK

    def test_patch_boards(self):
        boards = self.client.get(
            "/api/boards/",
        ).data
        board_id = boards[0]["id"]
        assert boards[0]["deleted"] is False
        response = self.client.patch(f"/api/boards/{board_id}/", data={"deleted": True})
        assert response.data["deleted"] is True
        assert response.status_code == status.HTTP_200_OK

    def test_delete_boards(self):
        boards = self.client.get(
            "/api/boards/",
        ).data
        board_id = boards[0]["id"]
        response = self.client.delete(
            f"/api/boards/{board_id}/",
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
