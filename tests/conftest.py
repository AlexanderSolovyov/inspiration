from django.test import Client

import pytest
from rest_framework.test import APIClient

from users.models import User


@pytest.fixture(scope="class")
@pytest.mark.django_db
def inspiration_user(request):
    class UserFixture:
        username = "test1"
        password = "test1"

        def __init__(self):
            self.test_user = User.objects.create(
                username=self.username,
                email="test1@mail.com",
                password=self.password,
                first_name="alex",
            )
            self.test_user.save()
            self.test_user.set_password(self.password)
            self.test_user.save()

        def get_patched_client(self):
            client = APIClient()
            response = client.post(
                "/api/users/login/",
                data={"username": self.username, "password": self.password},
            )
            token = response.json()["token"]
            client.credentials(HTTP_AUTHORIZATION="Token " + token)
            return client

    request.cls.fixtures = UserFixture()
