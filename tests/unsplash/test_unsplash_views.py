import requests_mock

from tests.base import InspirationTestCase
from tests.data.unsplash.responses import IMAGE_SEARCH_RESPONSE
from unsplash.models import SearchHistory


class UnsplashViewsTestCase(InspirationTestCase):
    def test_image_search_view(self):
        with requests_mock.Mocker() as mock_request:
            mock_request.register_uri(
                method="GET",
                url=f"https://api.unsplash.com/search/photos",
                json=IMAGE_SEARCH_RESPONSE,
            )

            response = self.client.get(
                f"/api/unsplash/search/",
                {"query": ["test_dog"], "order_by": ["relevant"], "page": ["1"]},
            )
            assert response.status_code == 200
            assert response.json() == IMAGE_SEARCH_RESPONSE

    def test_wrong_payload(self):
        with requests_mock.Mocker():
            response = self.client.get(
                f"/api/unsplash/search/",
                {"order_by": ["relevant"], "page": ["1"]},
            )
            assert response.status_code == 400
            assert response.json() == {"error": "query field required"}

            response = self.client.get(
                f"/api/unsplash/search/",
                {"query": ["test_dog"], "order_by": ["relevant"], "page": ["popa"]},
            )
            assert response.status_code == 400
            assert response.json() == {"error": "page must be int value"}

    def test_search_history(self):
        with requests_mock.Mocker() as mock_request:
            mock_request.register_uri(
                method="GET",
                url=f"https://api.unsplash.com/search/photos",
                json=IMAGE_SEARCH_RESPONSE,
            )

            assert SearchHistory.objects.all().count() == 0
            assert self.client.get("/api/users/search-history/").json() == []

            for i in range(15):
                self.client.get(
                    f"/api/unsplash/search/",
                    {
                        "query": [f"test_dog{i}"],
                        "order_by": ["relevant"],
                        "page": ["1"],
                        "orientation": ["landscape"],
                    },
                )
                assert SearchHistory.objects.filter(
                    user=self.fixtures.test_user, query=f"test_dog{i}"
                ).exists()
                assert (
                    SearchHistory.objects.filter(user=self.fixtures.test_user).count()
                    == i + 1
                )

                response = self.client.get("/api/users/search-history/").json()
                assert response[0].get("query") == f"test_dog{i}"
