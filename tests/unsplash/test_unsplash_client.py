import requests_mock

from tests.base import InspirationTestCase
from tests.data.unsplash.responses import IMAGE_SEARCH_RESPONSE
from unsplash.utils import UnsplashClient


class UnsplashClientTestCase(InspirationTestCase):
    adapter = None
    unsplash_client = None
    mock_path = "mock://test.com"

    def setUp(self):
        with self.settings(UNSPLASH_PUBLIC_KEY="test_key"):
            self.adapter = requests_mock.Adapter()
            self.unsplash_client = UnsplashClient()
            # self.adapter.register_uri(
            #     method="GET", url=f"https://api.unsplash.com/{self.mock_path}", json=IMAGE_SEARCH_RESPONSE
            # )

    def test_headers(self):
        """test unsplash_client headers are correct"""
        assert self.unsplash_client._build_headers() == {
            "Authorization": f"Client-ID test_key",
            "Accept-Version": "v1",
        }

    def test_retrieve_url(self):
        with requests_mock.Mocker() as mock_request:
            mock_request.register_uri(
                method="GET",
                url=f"https://api.unsplash.com/{self.mock_path}",
                json=IMAGE_SEARCH_RESPONSE,
            )
            assert (
                self.unsplash_client.retrieve_url(path=self.mock_path, params={})
                == IMAGE_SEARCH_RESPONSE
            )
            assert mock_request.call_count == 1
            assert mock_request.called
            request_sent = mock_request.request_history[0]
            assert request_sent.url == f"https://api.unsplash.com/{self.mock_path}"
            assert request_sent.headers.get("Authorization") == "Client-ID test_key"
            assert request_sent.headers.get("Accept-Version") == "v1"

    def test_search_image(self):
        with requests_mock.Mocker() as mock_request:
            mock_request.register_uri(
                method="GET",
                url=f"https://api.unsplash.com/search/photos",
                json=IMAGE_SEARCH_RESPONSE,
            )

            assert (
                self.unsplash_client.search_image(query="test_dog", page=1)
                == IMAGE_SEARCH_RESPONSE
            )
            assert mock_request.call_count == 1
            assert mock_request.called
            request_sent = mock_request.request_history[0]
            assert sorted(request_sent.qs) == sorted(
                {
                    "query": ["test_dog"],
                    "order_by": [None],
                    "orientation": [None],
                    "page": ["1"],
                }
            )
